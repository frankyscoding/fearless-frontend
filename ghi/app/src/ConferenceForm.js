import React, { useEffect, useState } from 'react'

function ConferenceForm() {
    const [locations, setLocations] = useState([])
    const [name, setName] = useState('')
    const [start, setStart] = useState('')
    const [end, setEnd] = useState('')
    const [description, setDescription] = useState('')
    const [maxpres, setMaxpres] = useState('')
    const [maxatten, setMaxatten] = useState('')
    const [location, setLocation] = useState('')

    const namechange = (event) => {
        const value = event.target.value;
        setName(value);
      }

    const startchange = (event) => {
        const value = event.target.value;
        setStart(value);
      }

    const endchange = (event) => {
        const value = event.target.value;
        setEnd(value);
      }

    const descriptionchange = (event) => {
        const value = event.target.value;
        setDescription(value);
      }

    const maxpreschange = (event) => {
        const value = event.target.value;
        setMaxpres(value);
      }
    const maxattenchange = (event) => {
        const value = event.target.value;
        setMaxatten(value);
      }
    const locationchange = (event) => {
        const value = event.target.value;
        setLocation(value);
      }

    const submitchange = async (event) => {
        event.preventDefault();

        const data = {}

        data.name = name
        data.starts = start
        data.ends = end
        data.description = description
        data.max_presentations = maxpres
        data.max_attendees = maxatten
        data.location = location

        console.log(data)
        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(conferenceUrl, fetchConfig)
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setStart('')
            setEnd('')
            setDescription('')
            setMaxpres('')
            setMaxatten('')
            setLocation('')
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
            setLocations(data.locations)

        }
      }

      useEffect(() => {
        fetchData();
      }, []);

  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={submitchange} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={namechange} placeholder="Name" required type="text" name ="name" id="name" className="form-control" value={name}/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={startchange} placeholder="Starts" required type="date" name ="starts" id="starts" className="form-control" value={start}/>
                <label htmlFor="start">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={endchange} placeholder="Ends" required type="date" name ="ends" id="ends" className="form-control" value={end}/>
                <label htmlFor="end">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description" className="form-label">Description</label>
                <textarea onChange={descriptionchange} className="form-control"  name="description" id="description" rows="3" value={description}></textarea>

              </div>
              <div className="form-floating mb-3">
                <input onChange={maxpreschange} placeholder="Maximum presentations" required type="number" name ="max_presentations" id="max_presentations" className="form-control" value={maxpres}/>
                <label htmlFor="maximum_presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={maxattenchange} placeholder="Maximum attendees" required type="number" name ="max_attendees" id="max_attendees" className="form-control" value={maxatten}/>
                <label htmlFor="maximum_attendees">Maximum attendees</label>
              </div>

              <div className="mb-3">
                <select onChange={locationchange} required id="location" name="location" className="form-select" value={location}>
                <option value="">Choose a location</option>
                {locations.map(location => {
                    return (
                        <option key={location.id} value={location.id}>
                            {location.name}
                        </option>
                );
                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
  )
}

export default ConferenceForm
