function createCard(name, description, pictureUrl, StartDate, EndDate, Location) {
    return `
    <div class="col-3">
      <div class="card shadow-lg" >
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${Location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        ${StartDate} - ${EndDate}
      </div>
      </div>
      </div>
    `;
  }

 function createAlert(Error) {
    return `
    <div class="alert alert-warning d-flex align-items-center" role="alert">
    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Warning:"><use xlink:href="#exclamation-triangle-fill"/></svg>
    <div>
      ${Error}
    </div>
  </div>`
 }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
        badresp = "Unable to fetch URL"
        const some = createAlert(badresp)
        const column = document.querySelector('.row');
            column.innerHTML += some;
      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
            const details = await detailResponse.json();
            console.log(details);
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const StartDate = details.conference.starts;
            const Start = new Date(StartDate).toLocaleDateString();
            const EndDate = details.conference.ends;
            const End  = new Date(EndDate).toLocaleDateString();
            const Location = details.conference.location.name;

            const html = createCard(title, description, pictureUrl, Start, End, Location);
            const column = document.querySelector('.row');
            column.innerHTML += html;
        }
      }

    }
  } catch (e) {
    const baderr = "There is an Error with data"
    const somes = createAlert(baderr)
    const column = document.querySelector('.row');
        column.innerHTML += somes;
    // Figure out what to do if an error is raised
  }

});
